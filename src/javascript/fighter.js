class Fighter{
    constructor(fighter){
        this.name = fighter.name;
        this.health = fighter.health;
        this.defense = fighter.defense;
        this.attack = fighter.attack;
        this.source = fighter.source;
    }

    getHitPower(){
        let criticalHitChance = randomInteger(1,2);
        let power = this.attack * criticalHitChance;
        return power;
    }

    getBlockPower(){
        let dodgeChance = randomInteger(1,2);
        let power  = this.defense * dodgeChance;
        return power;
    }
}

function randomInteger(min, max) {
    let rand = min - 0.5 + Math.random() * (max - min + 1)
    rand = Math.round(rand);
    return rand;
}

  export default Fighter;