import View from './view';
  import FighterView from './fighterView';
  import { fighterService } from './services/fightersService';
  import Fighter from './fighter'
import App from './app';

class FightersView extends View {
  constructor(fighters) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();
  selectedFighters = [];
  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  async handleFighterClick(event, fighter) {
    document.getElementsByClassName('fighters')[0].style.display = 'none';
    if(this.fightersDetailsMap.has(fighter._id)){
      this.appendStats(fighter);
    }
    else{
    const fighterDetails = await fighterService.getFighterDetails(fighter._id);
    this.fightersDetailsMap.set(fighter._id, fighterDetails);
    this.appendStats(fighter);
    console.log(this.fightersDetailsMap);
      }
    // get from map or load info and add to fightersMap
    // show modal with fighter info
    // allow to edit health and power in this modal
  } 
  appendStats(fighter){
    const stats = this.createStats(fighter);
    const root = document.getElementById('root'); 
    root.insertBefore(stats, root.firstChild);
    document.getElementById('statusFightName').innerHTML= this.fightersDetailsMap.get(fighter._id).name + "'s stats";
    document.getElementById('def').value = this.fightersDetailsMap.get(fighter._id).defense;
    document.getElementById('atc').value = this.fightersDetailsMap.get(fighter._id).attack;
    document.getElementById('hp').value = this.fightersDetailsMap.get(fighter._id).health;
    document.getElementById('btnOK').innerHTML="OK";
    document.getElementById('btnSelect').innerHTML="Select";
    document.getElementById('btnOK').onclick = ()=>{
      this.changeStats(fighter);
    }
    document.getElementById('btnSelect').onclick = ()=>{
      this.changeStats(fighter);

      const heroForFight = new Fighter(this.fightersDetailsMap.get(fighter._id));
      this.selectedFighters.push(heroForFight);
      if(this.selectedFighters.length==2){
        this.createBattlefield(this.selectedFighters[0],this.selectedFighters[1]);
        this.runFight(this.selectedFighters[0],this.selectedFighters[1]);
      }
    }
  }
  changeStats(fighter){
    const child = document.getElementById('stats'); 
    this.fightersDetailsMap.get(fighter._id).defense =  document.getElementById('def').value;
    this.fightersDetailsMap.get(fighter._id).attack = document.getElementById('atc').value;
    this.fightersDetailsMap.get(fighter._id).health =  document.getElementById('hp').value;
    document.getElementById('root').removeChild(child);
    document.getElementsByClassName('fighters')[0].style.display = 'flex';
  }
  
  createStats(fighter){
    const attributes = {id: "stats"}
    const defense = this.createDef();
    const gif = this.createGif(fighter.source);
    const attack = this.createAtc();
    const health = this.createHp();
    const name = this.createFigterName();
    const okButton = this.createOkButton();
    const selectButton = this.createSelectButton();
    const stats = this.createElement({
       tagName: 'div',
       className: 'stats',
       attributes
    });
    stats.append(name, gif, defense,attack,health,okButton,selectButton);
    
 
   return stats;
   }
   createFigterName(){
   const attributes = {id: 'statusFightName'}
   const fighterName = this.createElement({
     tagName: 'div',
     className: 'statusName',
     attributes
   })
   return fighterName;
   }
   createGif(source) {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });

    return imgElement;
  }
   createDef(){
    const attributes = {value : "10",id:'def', title: "defense stat"}
    const defStat = this.createElement({
     tagName: 'input',
     className: 'def',
     attributes
   });
   return defStat;
   }
   createAtc(){
    const attributes = {value : "10", id: "atc" , title: "attack stat"}
    const atcStat = this.createElement({
      tagName: 'input',
      className: 'atc',
      attributes
    });
    return atcStat;
   }
   createHp(){
    const attributes = {value : "10", id: "hp", title: "health stat"}
    const hpStat = this.createElement({
      tagName: 'input',
      className: 'hp',
      attributes
    });
    return hpStat;
   }
   createOkButton(){
    const attributes = {value : "10", id: "btnOK"}
    const okBtn = this.createElement({
      tagName:'button',
      className:'okBtn',
      attributes
    })

    return okBtn;
   }
   createSelectButton(){
    const attributes = {value : "10", id: "btnSelect"}
    const selectBtn = this.createElement({
      tagName:'button',
      className:'selectBtn',
      attributes
    })

    return selectBtn;
   }
   createBattlefield(hero1, hero2){
     let j = 0;
     let lengthFighters = document.getElementsByClassName('fighter').length;
    for(let i=0; i<lengthFighters; i++){
      if( document.getElementsByClassName('fighter')[i-j].children[1].innerHTML==hero1.name || document.getElementsByClassName('fighter')[i-j].children[1].innerHTML==hero2.name){
    }
    else{
    document.getElementsByClassName('fighter')[i-j].remove();
    j++;
  }
   }
   document.getElementsByClassName('fighters')[0].classList.remove('fighters');
   document.getElementById('root').children[0].classList.add('ready-for-fight');
   document.getElementsByClassName('fighter')[1].children[0].classList.add('image-fight');
   const winnerText = this.createWinnerText();
   document.getElementsByClassName('ready-for-fight')[0].insertBefore(winnerText,document.getElementsByClassName('ready-for-fight')[0].firstChild).style.display="none";
   document.getElementsByClassName('fighter')[0].insertBefore(this.createHealthBar(hero1), document.getElementsByClassName('fighter')[0].firstChild);
   document.getElementsByClassName('fighter')[1].insertBefore(this.createHealthBar(hero2), document.getElementsByClassName('fighter')[1].firstChild);
   const backBtn = this.createRestoreToDefaultButton();
   document.getElementsByClassName('ready-for-fight')[0].appendChild(backBtn).style.display="none";
   document.getElementById('btnBack').onclick = ()=>{
     document.getElementById('root').children[0].remove();
     new App();
   };
}
createHealthBar(fighter){
  const attributes = {value : fighter.health, max: fighter.health}
    const hpStat = this.createElement({
      tagName: 'progress',
      className: 'hpBar',
      attributes
    });
    return hpStat;
}
createWinnerText(){
  const winnerText = this.createElement({
      tagName: 'h1',
      className: 'winner-text'
    });
    return winnerText;
 }
createRestoreToDefaultButton(){
  const attributes = {id: "btnBack"}
    const backBtn = this.createElement({
      tagName:'button',
      className:'backBtn',
      attributes
    })

    return backBtn;
}
runFight(hero1, hero2){
     
  setInterval(fight, 1000, hero1, hero2);
}

}
function fight(hero1, hero2){
  let heroHitPower1;
  let heroHitPower2;
  let heroDefensePower1;
  let heroDefensePower2;
    heroDefensePower1 = hero1.getBlockPower();
    heroDefensePower2 = hero2.getBlockPower();
    heroHitPower1 =  hero1.getHitPower();
    heroHitPower2 =  hero2.getHitPower();
    if(heroHitPower1>heroDefensePower2){
      hero2.health -= heroHitPower1 - heroDefensePower2;
      if(document.getElementsByClassName('fighter')[1].children[2].innerHTML==hero2.name)
      document.getElementsByClassName('fighter')[1].children[0].value = hero2.health;
      else
      document.getElementsByClassName('fighter')[0].children[0].value = hero2.health;
      console.log(hero2.name+" : "+hero2.health);
  }
      if(hero2.health>0 && heroHitPower2>heroDefensePower1){
          hero1.health -= heroHitPower2 - heroDefensePower1;
          if(document.getElementsByClassName('fighter')[1].children[2].innerHTML==hero1.name)
      document.getElementsByClassName('fighter')[1].children[0].value = hero1.health;
      else
      document.getElementsByClassName('fighter')[0].children[0].value = hero1.health;
      }
      console.log(hero1.name+" : "+hero1.health);
  if(hero1.health<1||hero2.health<1){
      for(let i=0; i<100; i++){
  window.clearInterval(i); 
  }
  if(hero1.health>hero2.health)
  showWinner(hero1.name);
  else
  showWinner(hero2.name);

}
function showWinner(name){
  if(document.getElementsByClassName('fighter')[1].children[2].innerHTML==name){
      document.getElementsByClassName('fighter')[0].remove();
  }
  else{
      document.getElementsByClassName('fighter')[1].remove();
  }
  document.getElementsByClassName('winner-text')[0].style.display="block";
  document.getElementsByClassName('winner-text')[0].innerHTML = name + "'s victory";
  document.getElementsByClassName('backBtn')[0].style.display="block";
  document.getElementsByClassName('backBtn')[0].innerHTML="Back to hero menu"
}  
}



export default FightersView;